#include "xAODJet/Jet.h"
#include "xAODBTagging/BTagging.h"

class JetSelectionHelper {

public:

  // constructor
  JetSelectionHelper() {};

  // destructor
  virtual ~JetSelectionHelper() {};

  // checks the kinematics of the object
  bool isJetGood(const xAOD::Jet* jet);

  // checks if the jet is a b-jet
  bool isJetBFlavor(const xAOD::Jet* jet);

};
